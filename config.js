module.exports = {
  port: 3005,
  //tls: {
  //  sslPort: 443,
  //  cert: "/certs/server.crt",
  //  key: "/certs/server.key"
  //},
  log: true,
  // MongoDB is optional. Comment this section out if not desired.
  mongo: {
    databaseUrl: "mongodb://localhost:27117/skynet_dev"
  },
  // REDIS is optional. It's used for scaling session and sockets horizontally. Comment this section out if not desired.
  // redis: {
  //   host: "xyz.redistogo.com",
  //   port: "1234",
  //   password: "abcdef"
  // },
  // ElasticSearch is optional. It's used to analyze data. Comment this section out if not desired.
  // elasticSearch: {
  //   host: "localhost",
  //   port: "9200"
  // },
  // this skynet cloud's uuid
  uuid: '38D41ABB-1586-410F-840C-F28630FA1E15',
  token: '112B4C8EF896438A8C33126ED6B10EDB',
  broadcastActivity: true,  
  // if you want to resolve message up to another skynet server:
  //parentConnection: {
  //  uuid: '8d1b4430-1d49-11e4-8eee-3d689a654d62',
  //  token: '0vun4pzdqrv841jor2ebx16za0wfjemi',    
  //  server: '193.174.152.236',
  //  port: 3002
  //},
  rateLimits: {
    message: 10, // 10 transactions per user per second
    data: 10, // 10 transactions per user per second
    connection: 2, // 2 transactions per IP per second
    query: 2, // 2 transactions per user per second
    whoami: 10, // 10 transactions per user per second
    unthrottledIps: ["193.174.152.236"] // allow unlimited transactions from these IP addresses
  },
  plivo: {
    authId: "abc",
    authToken: "123"
  },
  urbanAirship: {
    key: "abc",
    secret: "123"
  },
  coap: {
    port: 5683,
    host: "localhost"
  },
  //these settings are for the mqtt server, and skynet mqtt client
  mqtt: {
 //   databaseUrl: "mongodb://user:pass@adam.mongohq.com:1337/skynet",
    port: 1883,
    skynetPass: "Very big random password 34lkj23orfjvi3-94ufpvuha4wuef-a09v4ji0rhgouj"
  },
  yo: {
    token: "your yo token from http://yoapi.justyo.co/"
  }
};
